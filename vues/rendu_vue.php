<div class="panneau_details">
	<?php

	if (isset($IdPRendu) && isset($NomEq)) {
		echo '<h2>Le nom du projet est ' . $Nom . ' et son descriptif : ' . $Descriptif . '</h2>';
		echo '<br>';
		echo '<table class="table_resultat">';
		echo '<thead>';
		echo '<tr>';
		echo '<th>IdJ</th>';
		echo '<th>Date de rendu</th>';
		echo '<th>Type</th>';
		echo '</tr>';
		echo '</thead>';
		echo '<tbody>';
		if (isset($JalonIdPF)) {
			foreach ($JalonIdPF as $j) {
				echo '<tr><td> <a href ="index.php?page=rendu&IdJChoisi=' . $j["IdJ"] . '&TypeJ=' . $j["Type"] . '&IdPRendu=' . $IdPRendu . '" >' . $j["IdJ"] . ' </td><td> ' . $j["DateRendu"] . ' </td><td> ' . $j["Type"] . '</a>' . '</td></tr>';
			}
		}
		echo '</tbody>';
		echo '</table>';
	}



	if (isset($_GET['IdJChoisi']) && isset($_GET['TypeJ'])) {
		echo '<form class="bloc_commandes" method="POST" action="#" enctype="multipart/form-data">';

		if ($_GET['TypeJ'] == "Etat Avancement") {
			echo '<label for="Descriptif avancement">Description : </label>';
			echo '<input type="text" name="descAv" placeholder="Décrire votre page d’accueil"> <br> <br>';

			echo '<br>Téléverser votre archive :';
			echo '<input type="file" name="RenduAv" accept=".zip, .tar, .gz">';
		}

		if ($_GET['TypeJ'] == "Rapport Final") {
			echo '<label for="Titre">Titre : </label> ';
			echo '<input type="text" name="titreRapp" placeholder="Insérer le titre" size=40> <br> <br>';

			echo '<label for="Descriptif">Description : </label>';
			echo '<input type="text" name="descRapp" placeholder="Décrire les objectifs de votre rapport" size=90>';
		}
		echo '<br><button type="submit" name="BoutonChoixRendu" class="btn-req" style = "width : 80px; height : 50px" >Déposer le rendu</button>';
		echo '</form>';
	}

	if (isset($_POST['BoutonChoixRendu'])) {
		if (isset($_FILES['RenduAv']['tmp_name'])) {
			$r = move_uploaded_file($_FILES['RenduAv']['tmp_name'], "../Rendu/IdJ=".$_GET['IdJChoisi'].'_IdP='.$_GET['IdPRendu']);
			if ($r) {
				echo "Transfert réussi !";
			} else {
				echo "Transfert impossible. Vérifiez le format de votre fichier svp ...";
			}
		}
	}
	?>
</div>