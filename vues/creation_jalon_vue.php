<div class="panneau_details">
    <h2>Définissez les jalons du projet</h2>
    <form class="bloc_commandes" method="POST" action ="index.php?page=creation_jalon&IdEns=<?php echo $_GET['IdEns'].'&IdP='.$_GET['IdP'].'&IdEP='.$_GET['IdEP']; ?>">
        Sélectionnez le type de jalon à ajouter :
        <select name="typeJalon">
            <option value="Etat Avancement">Etat Avancement</option>
            <option value="Rapport Final">Rapport Final</option>
            <option value="Soutenance Finale">Soutenance Finale</option>
            <option value="Revue de code">Revue de code</option>
        </select>
        <br>
        Date de rendu : <input type="date" name="DateRendu"><br>
        Noté :
        Vrai<input type="radio" name="NoteBool" value="1">
        Faux<input type="radio" name="NoteBool" value="0">
        <br>
        <input type="submit" name="AjouterJalon" value="Ajouter" formaction ="index.php?page=creation_jalon&IdEns=<?php echo $_GET['IdEns'].'&IdP='.$_GET['IdP'].'&IdEP='.$_GET['IdEP']; ?>">
        <br>
        <input type="submit" name="TerminerJalon" value="Terminer la création de jalon">
    </form>
</div>