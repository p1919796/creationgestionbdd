<div class="panneau">

	<div>
		<!-- Bloc permettant d"afficher les statistiques -->

		<h2>Statistiques de la base</h2>

		<table class="table_resultat">
			<thead>
				<tr>
					<th>Propriété</th>
					<th>Valeur</th>
				</tr>
			</thead>
			<tbody>

				<tr>
					<td>Nombre de tables</td>
					<td> <?php foreach ($stats as $r) {
								echo $r["COUNT(TABLE_NAME)"];
							}  ?></td>
				</tr>
				<tr>
					<td>Nombre de tuples</td>
					<td> <?php foreach ($stats as $r) {
								echo $r["SUM(TABLE_ROWS)"];
							} ?></td>
				</tr>

			</tbody>
		</table>

		<h3>Liste des projets actifs</h3>

		<table class="table_resultat">
			<thead>
				<tr>
					<th>IdP</th>
					<th>Libellé</th>
					<th>Résumé</th>
					<th>Etat</th>
					<th>Année</th>
					<th>Semestre</th>
				</tr>
			</thead>
			<tbody>


				<?php
				if (isset($StatProjF)) {
					foreach ($StatProjF as $s) {
						echo "<tr><td>" . $s["IdP"] . " </td><td> " . $s["Libellé"] . " </td><td> " . $s["Résumé"] . " </td><td> " . $s["Etat"] . " </td><td> " . $s["Année"] . " </td><td> " . $s["Semestre"] . "</td></tr>";
					}
				}
				?>

			</tbody>
		</table>

		<br>

		<table class="table_resultat">
			<thead>
				<tr>
					<th>Sigle UE</th>
					<th>Libellé UE</th>
					<th>Nombre de projets (Actif / Archivé)</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if (isset($UEF)) {
					foreach ($UEF as $s) {
						echo "<tr><td>" . $s["Sigle"] . " </td><td> " . $s["Libellé"] . " </td><td> " . $s["NbPrjtAct"] . " / " . $s["NbPrjtArch"] . " </td></tr>";
					}
				}
				?>
			</tbody>
		</table>

		<br>

		<?php 
		foreach ($UEMPF as $s) {
			echo 'L"UE ayant proposé le plus de projet a pour Code APOGE :' . $s["CodeAPOGE"] . ' , Libellé : ' . $s["Libellé"] .
				' ,Sigle : ' . $s["Sigle"] . '. Son nombre de projet total est de : ' . $s["nb"];
		}

		echo '<br>';

		foreach ($UeEqPlus2EtF as $Plus2) {
			echo "Le nombre d'UE qui accepte les équipes de plus de 2 étudiants est : " . $Plus2["NbUEEqPlusde2"] . "<br>";
		}

		echo "Le(s) enseignant(s) ayant encadré le plus de projet est(sont) : ";
		foreach ($EnsEncaF as $e) {
			echo $e["Nom"] . "  " . $e["Prénom"] . "<br>";
		}
		?>

		<br>

		<table class="table_resultat">
			<thead>
				<tr>
					<th>Sigle UE</th>
					<th>Libellé UE</th>
					<th>IdP</th>
					<th>Année</th>
					<th>Semestre</th>
					<th>Nom de l'étudiant</th>
					<th>Prénom de l'étudiant</th>
					<th>Note obtenu</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if (isset($SLNomPrenomMNF)) {
					foreach ($SLNomPrenomMNF as $s) {
						echo "<tr><td>" . $s["Sigle"] . " </td><td> " . $s["Libellé"] . " </td><td> " . $s["IdP"] . " </td><td> " . $s["Année"] . " </td><td> " . $s["Semestre"] . " </td><td> " . $s["Nom"] . " </td><td> " . $s["Prénom"] . " </td><td> " . $s["NoteFinale"] . " </td></tr>";
					}
				}
				?>
			</tbody>
		</table>


	</div>

</div>