<div class="panneau">
<div>
	<h2>Détails des jalons concernant le projet <?php echo $_GET['IdPChoisi'] . ' encadré par ' . $_GET['ensValeurch'] ?></h2>

	<table class="table_resultat">
		<thead>
			<tr>
				<th>IdP</th>
				<th>IdJ</th>
				<th>DateRendu</th>
				<th>Type</th>
				<th>Note Finale</th>
				<th>Nombre de rendu deposé</th>
				<th>Nombre de rendu total</th>
			</tr>
		</thead>
		<tbody>
			<?php
			if (isset($JalonF)) {
				foreach ($JalonF as $j) {
					echo '<tr><td>' . $j["IdP"] . ' </td><td> ' . $j["IdJ"] . ' </td><td> ' . $j["DateRendu"] . ' </td><td> ' . $j["Type"] . ' </td><td> ' . $j["NoteFinale"] . ' </td><td> ' . $j["NbRendu"] . ' </td><td> ' . $j["NbTotal"] . '</td></tr>';
				}
			}
			?>
		</tbody>
	</table>

	<br>

	<table class="table_resultat">
		<thead>
			<tr>
				<th>Nom</th>
				<th>IdJ</th>
				<th>Etat</th>
			</tr>
		</thead>
		<tbody>
			<?php
			if (isset($ListeEqPrjtF)) {
				foreach ($ListeEqPrjtF as $l) {
					echo '<tr><td>' . $l["Nom"] . ' </td><td> ' . $l["IdJ"] . ' </td><td> '; if($l["Etat"] == "Rendu"){ echo '<span style="color: #41cf0f">'.$l["Etat"].'</span></td></tr>'; }  else  echo '<span style="color: #f6b60c">'.$l["Etat"].'</span></td></tr>';
				}
			}
			?>
		</tbody>
	</table>
	</div>
</div>