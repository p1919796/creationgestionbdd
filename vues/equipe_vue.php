<div class="panneau_details">
	<form class="bloc_commandes" method="POST" action="#">

		<label for="typeVueEquipe"></label>
		<select name="EqValeur" id="typeVue">
			<?php
			foreach ($eqF as $e) {
				echo '<option value="' . $e["Nom"] . '">' . $e["Nom"] . '</option>';
			}
			?>
		</select>

		<input type="submit" name="boutonChoixEq" value="Choisir" />
	</form>

	<h2>Détails des projets de l'equipe</h2>

	<table class="table_resultat">
		<thead>
			<tr>
				<th>IdP</th>
				<th>Libellé</th>
				<th>Etat</th>
				<th>Année</th>
				<th>Semestre</th>
			</tr>
		</thead>
		<tbody>
			<?php if (isset($eqChPrjtF)) {
				foreach ($eqChPrjtF as $e) {
					echo '<tr><td> <a href ="index.php?page=rendu&IdPRendu=' . $e["IdP"] . '&EqCh=' . $eqCh . '" >' . $e["IdP"] . ' </td><td> ' . $e["Libellé"] . ' </td><td> ' . $e["Etat"] . ' </td><td> ' . $e["Année"] . ' </td><td> ' . $e["Semestre"] . '</a></td></tr>';
				}
			}
			?>
		</tbody>
	</table>
</div>