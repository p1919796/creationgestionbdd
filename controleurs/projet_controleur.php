<?php

$msg;
$liste = [];

$respUEReq = 'SELECT ens.Nom, ens.Prénom FROM Enseignant ens WHERE ens.IdEns IN (SELECT IdEns FROM est_responsable) ORDER BY ens.Nom, ens.Prénom;';
$respUERes = mysqli_query($connexion, $respUEReq);
$respUEF = mysqli_fetch_all($respUERes, MYSQLI_ASSOC);

$ListeUEReq = 'SELECT CodeAPOGE, Libellé FROM UE;';
$ListeUERes = mysqli_query($connexion, $ListeUEReq);
$ListeUEF = mysqli_fetch_all($ListeUERes, MYSQLI_ASSOC);

$MIdP = 'SELECT MAX(IdP)+1 as IdP FROM Projet;';
$MIdPRes = mysqli_query($connexion, $MIdP);
$MIdPF = mysqli_fetch_all($MIdPRes, MYSQLI_ASSOC);

foreach ($MIdPF as $m) $IdPIns = (int)$m['IdP'];

if (isset($_POST['boutonAjoutResp'])) {
    if (isset($_POST['NomNouvResp']) && isset($_POST['PrénomNouvResp'])) {
        $VerifPresentEns = 'SELECT ens.IdEns FROM Enseignant ens WHERE ens.Nom = "' . $_POST['NomNouvResp'] . '" AND ens.Prénom = "' . $_POST['PrénomNouvResp'] . '" ;';
        $VerifPresentEnsRes = mysqli_query($connexion, $VerifPresentEns);
        $VerifPresentEF = mysqli_fetch_row($VerifPresentEnsRes);
        //On vérifie la présence du nom dans la table Enseignant, s'il n'est pas présent, nous pouvons l'insérer 
        //s'il est présent, un message d'avertissement est renvoyé

        if ($VerifPresentEF == null) //s'il n'est pas présent, on l'insere dans la table enseignant et est_responsable avec son UE correpondante
        {
            $InsEnsReq = 'INSERT INTO Enseignant(Nom, Prénom) VALUES ("' . $_POST['NomNouvResp'] . '", "' . $_POST['PrénomNouvResp'] . '");';
            mysqli_query($connexion, $InsEnsReq);

            $IdEnsReq = 'SELECT IdEns FROM Enseignant WHERE Nom = "' . $_POST['NomNouvResp'] . '" AND Prénom = "' . $_POST['PrénomNouvResp'] . '" ;';
            $IdEnsRes = mysqli_query($connexion, $IdEnsReq);
            $IdEnsF = mysqli_fetch_all($IdEnsRes, MYSQLI_ASSOC);


            foreach ($IdEnsF as $i) $IdEnsR = $i["IdEns"];

            $InsRespReq = 'INSERT INTO est_responsable VALUES ("' . $IdEnsR . '", "' . $_POST['UEValeur'] . '");';
            mysqli_query($connexion, $InsRespReq);
        } else {
            $msg = "Vous êtes déjà inscrit en tant que responsable d'UE. Veuillez vérifiez votre nom svp.";
        }
    }
}

if (isset($_POST['boutonChoixResp'])) { //Si on a choisit un enseignant dans la liste
    if (isset($_POST['RespUEValeur'])) {
        $nomEnca = explode(" ", $_POST['RespUEValeur']);
        $IdEnsNomEncaReq = 'SELECT IdEns FROM Enseignant WHERE Nom ="' . $nomEnca[0] . '" AND Prénom = "' . $nomEnca[1] . '";';
        $IdEnsNomEncaRes = mysqli_query($connexion, $IdEnsNomEncaReq);
        $IdEnsNomEncaF = mysqli_fetch_all($IdEnsNomEncaRes, MYSQLI_ASSOC);

        foreach ($IdEnsNomEncaF as $ie) $IdEnsEnca = $ie['IdEns'];
    }
}

if (isset($_POST['boutonCreerPjt'])) { //Si on a appuyé sur le bouton de création de projet
    $UESaisie = $_POST['UEcreationPjt'];
    $LibSaisie = $_POST['LibelléPj'];
    $ResSaisie = $_POST['ResPj'];
    $LienSaisie = $_POST['LienPj'];
    $AnneeSaisie = (int)$_POST['AnnéePj'];
    $SeSaisie = $_POST['SemestrePj'];
    $NbSaisie = $_POST['NbMembrePj'];

    if (
        $LibSaisie != "" && $ResSaisie != "" && $LienSaisie != "" && $AnneeSaisie != ""
        && $SeSaisie != ""
    ) {

        $InsPrjtReq = 'INSERT INTO Projet(IdP, Libellé, Résumé, Lien, Etat, Année, Semestre) 
        VALUES (' . $IdPIns . ', "' . $LibSaisie . '", "' . $ResSaisie . '", "' . $LienSaisie . '", "Actif" , ' . $AnneeSaisie . ', "' . $SeSaisie . '") ;';
        $InsRedPrjtReq = 'INSERT INTO rédiger(IdEns, IdP) VALUES (' . $_GET['IdEns'] . ', ' . $IdPIns . ');';



        if (mysqli_query($connexion, $InsPrjtReq) && mysqli_query($connexion, $InsRedPrjtReq)) {
            $InsEtat = true;
            $msgIns = "La création du projet a bien été effectué.";
        } else {
            $InsEtat = false;
            $msgIns = "Une erreur est survenue lors de la création. Vérifiez et réessayez svp.";
        }

        $InsEPReq = 'INSERT INTO constituer(IdEns) VALUES (' . $_GET['IdEns'] . ');';
        if (mysqli_query($connexion, $InsEPReq)) {

            $IDEPRespReq = 'SELECT MAX(IdEP) as IdEP FROM constituer;';
            $IDEPRespRes = mysqli_query($connexion, $IDEPRespReq);
            $IDEPRespF = mysqli_fetch_all($IDEPRespRes, MYSQLI_ASSOC);

            foreach ($IDEPRespF as $i) $IDEP = $i['IdEP'];
        }





        for ($m = 0; $m <= 1; $m++) {
            $IdENv = 'SELECT MAX(IdE)+1 FROM Equipe;';
            $IdENvRes = mysqli_query($connexion, $IdENv);
            $IdENvF = mysqli_fetch_assoc($IdENvRes);


            $creerNvEq = 'INSERT INTO Equipe(IdE, Nom, IdEns) VALUES (' . $IdENvF['MAX(IdE)+1'] . ',"Equipe_' . $IdENvF['MAX(IdE)+1'] . '",' . $_GET['IdEns'] . ');';
            mysqli_query($connexion, $creerNvEq);

            for ($j = 1; $j <= $NbSaisie; $j++) {
                $NumEtu = 'SELECT NumEtu
	            FROM inscrit
	            WHERE "' . $UESaisie . '"= CodeAPOGE '; // Tirer un numetu au hasard

                $NumEtur = mysqli_query($connexion, $NumEtu);
                $tire = rand(0, mysqli_num_rows($NumEtur) - 1); // On tire une ligne au hasard
                mysqli_data_seek($NumEtur, $tire);
                $Nf = mysqli_fetch_assoc($NumEtur);

                if (!est_deja_choisi($liste, $Nf['NumEtu'])) {
                    array_push($liste, $Nf['NumEtu']);

                    $req = 'INSERT INTO appartenir_compose(IdE, NumEtu) VALUES (' . $IdENvF['MAX(IdE)+1'] . ',' . $Nf['NumEtu'] . ');';
                    mysqli_query($connexion, $req);

                    $reqRealiser = 'INSERT INTO réaliser(IdE, IdP) VALUES (' . $IdENvF['MAX(IdE)+1'] . ',' . $_GET['IdP'] . ');';
                    mysqli_query($connexion, $reqRealiser);
                }
            }
        }
    }
}

if (isset($_POST['boutonAjouterEnca'])) {
    if (isset($_GET['IdEP'])) $IdEPEnCours = $_GET['IdEP'];
    $nomEncaAajouter = explode(" ", $_POST['NvEncaEPValeur']);
    $IdEnsNomEncaAjReq = 'SELECT IdEns FROM Enseignant WHERE Nom ="' . $nomEncaAajouter[0] . '" AND Prénom = "' . $nomEncaAajouter[1] . '";';
    $IdEnsNomEncaAjRes = mysqli_query($connexion, $IdEnsNomEncaAjReq);
    $IdEnsNomEncaAjF = mysqli_fetch_all($IdEnsNomEncaAjRes, MYSQLI_ASSOC);

    foreach ($IdEnsNomEncaAjF as $ie) $IdEnsEncaAaj = $ie['IdEns'];

    $AjNvEnca = 'INSERT INTO constituer(IdEP, IdEns) VALUES (' . $_GET['IdEP'] . ', ' . $IdEnsEncaAaj . ');';
    if (mysqli_query($connexion, $AjNvEnca)) {
        $msgInsEnca = "L'encadrant a bien été ajouté.";
    } else {
        $msgInsEnca = "L'encadrant n'a pu être ajouté.";
    }
}