<?php
$IdPRendu = $_GET['IdPRendu'];

if(isset($_GET['EqCh'])) $NomEq = $_GET['EqCh'];


if (isset($_GET['IdPRendu']) && isset($_GET['EqCh'])) {
    

    //Pour trouver le nom et le descriptif du projet
    $LibDescPrjtReq = 'SELECT p.Libellé, p.Résumé
    FROM Projet p  WHERE p.IdP = ' . $IdPRendu . ' ;';

    $LibDescPrjtRes = mysqli_query($connexion, $LibDescPrjtReq);
    $LibDescPrjtF = mysqli_fetch_all($LibDescPrjtRes, MYSQLI_ASSOC);


    foreach ($LibDescPrjtF as $l) {
        $Nom = $l["Libellé"];
        $Descriptif = $l['Résumé'];
    }



    //Pour obtenir les jalons du projet en question
    $JalonIdPReq = 'SELECT j.IdJ, j.DateRendu, j.Type
    FROM Jalon j  WHERE j.IdP = ' . $IdPRendu . ' ;';

    $JalonIdPRes = mysqli_query($connexion, $JalonIdPReq);
    $JalonIdPF = mysqli_fetch_all($JalonIdPRes, MYSQLI_ASSOC);    
}

if(isset($_GET['IdJChoisi']) && isset($_GET['TypeJ']))
    {
        if($_GET['TypeJ'] == "Etat Avancement")
        {
            if (isset($_POST['BoutonChoixRendu'])) {
            $ReqIns = 'INSERT INTO Rendu (IdP, IdJ, DateRendu, Etat) VALUES ('.$IdPRendu.','.$_GET['IdJChoisi'].', "'.date("Y-m-d").'", "Rendu");';

            $Date = 'SELECT DateRendu FROM Jalon WHERE IdP = '.$IdPRendu.' AND IdJ = '.$_GET['IdJChoisi'].' AND Type = "'.$_GET['TypeJ'].'";';
            $DateR = mysqli_query($connexion, $Date);
            $DateF = mysqli_fetch_assoc($DateR);

            $JalIns = 'INSERT INTO Avancement (IdP, IdJ, DateInitiale, DateRendu) VALUES ('.$IdPRendu.','.$_GET['IdJChoisi'].', "'.$DateF['DateRendu'].'","'.$DateF['DateRendu'].'");';
            $JalEltIns = 'INSERT INTO élément (texte) VALUES ("'.$_POST['descAv'].'");';
            mysqli_query($connexion, $JalEltIns);
            $DerElt = 'SELECT MAX(IdElt) as IdIns FROM élément;';
            $DerEltR = mysqli_query($connexion, $DerElt);
            $DerEltF = mysqli_fetch_assoc($DerEltR);
            $CompEltIns = 'INSERT INTO compose(IdP ,IdElt, IdJ) VALUES ('.$IdPRendu.','.intval($DerEltF['IdIns']).','.$_GET['IdJChoisi'].');';

             //On effectue les insertion dans l'ordre
            mysqli_query($connexion, $CompEltIns);
            mysqli_query($connexion, $ReqIns);
            mysqli_query($connexion, $JalIns);
            }
        }

        if($_GET['TypeJ'] == "Rapport Final")
        {
            if (isset($_POST['BoutonChoixRendu'])) {
            $ReqIns = 'INSERT INTO Rendu (IdP, IdJ, DateRendu, Etat) VALUES ('.$IdPRendu.','.$_GET['IdJChoisi'].', "'.date("Y-m-d").'", "Rendu");';

            $Date = 'SELECT DateRendu FROM Jalon WHERE IdP = '.$IdPRendu.' AND IdJ = '.$_GET['IdJChoisi'].' AND Type = "'.$_GET['TypeJ'].'";';
            $DateR = mysqli_query($connexion, $Date);
            $DateF = mysqli_fetch_assoc($DateR);


            $JalIns = 'INSERT INTO Rapport (IdP, IdJ, DateInitiale, DateRendu, titre, description) VALUES ('.$IdPRendu.','.$_GET['IdJChoisi'].', "'.$DateF['DateRendu'].'" , "'.$DateF['DateRendu'].'" ,"'.$_POST['titreRapp'].'", "'.$_POST['descRapp'].'" );';
             //On effectue les insertion dans l'ordre
            mysqli_query($connexion, $ReqIns);
            mysqli_query($connexion, $JalIns);
            }
        }
    }