<?php 
$message = "";

// recupération de la liste des tables
$stats = get_statistiques();

if($stats == null || count($stats) == 0) {
	$message .= "Aucune statistique n'est disponible!";
}else{
	if(isset($nbT) && isset($nbLi))
	{
		$nbT  = $stats["COUNT(TABLE_NAME)"];
		$nbLi = $stats["SUM(TABLE_ROWS)"];
	}else  echo $message;
}

$StatProjReq = 'SELECT IdP, Libellé, Résumé, Etat, Année, Semestre FROM Projet WHERE Etat = "Actif" ;';
$StatProjRes = mysqli_query($connexion, $StatProjReq);
$StatProjF = mysqli_fetch_all($StatProjRes, MYSQLI_ASSOC);


$UEReq = 'SELECT u.Sigle, u.Libellé, 
(SELECT COUNT(p.IdP) FROM Projet p JOIN réaliser rea ON rea.IdP = p.IdP JOIN Equipe eq ON rea.IdE = eq.IdE 
JOIN Enseignant ens ON ens.IdEns = eq.IdEns JOIN est_responsable er ON er.IdEns = ens.IdEns JOIN UE u2 ON 
er.CodeAPOGE = u.CodeAPOGE WHERE Etat = "Actif" AND u2.Libellé = u.Libellé) as "NbPrjtAct",
(SELECT COUNT(p.IdP) FROM Projet p JOIN réaliser rea ON rea.IdP = p.IdP JOIN Equipe eq ON rea.IdE = eq.IdE 
JOIN Enseignant ens ON ens.IdEns = eq.IdEns JOIN est_responsable er ON er.IdEns = ens.IdEns JOIN UE u1 ON 
er.CodeAPOGE = u.CodeAPOGE WHERE Etat = "Archivé" AND u1.Libellé = u.Libellé) as "NbPrjtArch"
FROM UE u, Projet p, est_responsable er,
Enseignant ens, Equipe eq, réaliser re WHERE u.CodeAPOGE = er.CodeAPOGE AND er.IdEns = ens.IdEns 
AND eq.IdEns = ens.IdEns AND eq.IdE = re.IdE AND re.IdP = p.IdP GROUP BY u.Libellé;';
$UERes = mysqli_query($connexion, $UEReq);
$UEF = mysqli_fetch_all($UERes, MYSQLI_ASSOC);





$UEMPReq = 'SELECT CodeAPOGE, Libellé, Sigle, nb FROM nbpjt WHERE nb = (SELECT MAX(nb) from nbpjt);';
$UEMPRes = mysqli_query($connexion, $UEMPReq);
$UEMPF = mysqli_fetch_all($UEMPRes, MYSQLI_ASSOC);

//Pour le nombre d'ue qui aacepte des eq de plus de 2 etudiants
$UeEqPlus2EtReq = 
'SELECT COUNT(distinct u.CodeAPOGE) as "NbUEEqPlusde2" 
FROM Equipe eq JOIN Enseignant ens ON eq.IdEns = ens.IdEns JOIN est_responsable er ON ens.IdEns = er.IdEns 
JOIN UE u ON er.CodeAPOGE = u.CodeAPOGE WHERE eq.IdE IN 
(SELECT IdE 
FROM appartenir_compose a 
WHERE (SELECT COUNT(b.NumEtu) 
FROM appartenir_compose b 
WHERE a.IdE = b.IdE) > 2);';
$UeEqPlus2EtRes = mysqli_query($connexion, $UeEqPlus2EtReq);
$UeEqPlus2EtF = mysqli_fetch_all($UeEqPlus2EtRes, MYSQLI_ASSOC);




$EnsEncaReq = 

'SELECT ens.Nom, ens.Prénom FROM Enseignant ens WHERE ens.IdEns IN (SELECT IdEns FROM EnsEncadrePlusProjet WHERE NbDeProjetEncadré = (SELECT MAX(NbDeProjetEncadré) FROM EnsEncadrePlusProjet));';
$EnsEncaRes = mysqli_query($connexion, $EnsEncaReq);
$EnsEncaF = mysqli_fetch_all($EnsEncaRes, MYSQLI_ASSOC);


$NomMeilleurNoteReq = 'SELECT e.Nom, e.Prénom FROM Etudiant e JOIN appartenir_compose ac ON e.NumEtu = ac.NumEtu JOIN Equipe eq ON ac.IdE = eq.IdE WHERE eq.IdE = (SELECT re.IdE FROM Réalisation re WHERE re.NoteFinale = (SELECT MAX(re2.NoteFinale) FROM Réalisation re2));';
$NomMeilleurNoteRes = mysqli_query($connexion, $NomMeilleurNoteReq);
$NomMeilleurNoteF = mysqli_fetch_all($NomMeilleurNoteRes, MYSQLI_ASSOC);

$SLNomPrenomMNReq = 
'
SELECT u.Sigle, u.Libellé, re.IdP, p.Année, p.Semestre, et.Nom, et.Prénom, re.NoteFinale 
FROM Réalisation re JOIN Equipe eq ON re.IdE = eq.IdE 
JOIN Enseignant ens ON eq.IdEns = ens.IdEns 
JOIN est_responsable er ON ens.IdEns = er.IdEns 
JOIN UE u ON er.CodeAPOGE = u.CodeAPOGE 
JOIN appartenir_compose ac ON eq.IdE = ac.IdE 
JOIN Etudiant et ON ac.NumEtu = et.NumEtu 
JOIN Projet p ON re.IdP = p.IdP
WHERE re.NoteFinale IN 
(SELECT MAX(re1.NoteFinale) 
FROM Réalisation re1 JOIN Equipe eq1 ON re1.IdE = eq1.IdE 
JOIN Enseignant ens1 ON eq1.IdEns = ens1.IdEns 
JOIN est_responsable er1 ON ens1.IdEns = er1.IdEns 
JOIN UE u1 ON er1.CodeAPOGE = u1.CodeAPOGE 
JOIN appartenir_compose ac1 ON eq1.IdE = ac1.IdE 
JOIN Etudiant et1 ON ac1.NumEtu = et1.NumEtu 
GROUP BY u1.CodeAPOGE ) 
GROUP BY u.CodeAPOGE;
';
$SLNomPrenomMNRes = mysqli_query($connexion, $SLNomPrenomMNReq);
$SLNomPrenomMNF = mysqli_fetch_all($SLNomPrenomMNRes, MYSQLI_ASSOC);

?>
