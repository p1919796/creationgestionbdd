#BENZIANE Abdeldjallil p1919796

CREATE TABLE Enseignant(
   IdEns INT AUTO_INCREMENT,
   Nom VARCHAR(50),
   Prénom VARCHAR(50),
   Statut VARCHAR(50),
   PRIMARY KEY(IdEns)
);

CREATE TABLE Etudiant(
   NumEtu INT,
   Nom VARCHAR(50),
   Prénom VARCHAR(50),
   PRIMARY KEY(NumEtu)
);

CREATE TABLE Projet(
   IdP INT,
   Libellé VARCHAR(50),
   Résumé VARCHAR(50),
   Lien VARCHAR(50),
   Etat VARCHAR(50),
   Année BIGINT,
   Semestre VARCHAR(50) NOT NULL,
   PRIMARY KEY(IdP)
);

CREATE TABLE Jalon(
   IdP INT,
   IdJ INT,
   DateInitiale DATE,
   DateRendu DATE,
   Noté BOOLEAN,
   Type VARCHAR(50),
   NoteFinale FLOAT,
   PRIMARY KEY(IdP, IdJ),
   FOREIGN KEY(IdP) REFERENCES Projet(IdP)
);

CREATE TABLE UE(
   CodeAPOGE VARCHAR(50),
   Libellé VARCHAR(50),
   Sigle VARCHAR(50),
   Niveau VARCHAR(50),
   Semestre VARCHAR(50),
   NbDeProjet INT,
   PRIMARY KEY(CodeAPOGE)
);

CREATE TABLE Equipe(
   IdE INT AUTO_INCREMENT,
   Nom VARCHAR(50),
   NbMembre INT,
   IdEns INT NOT NULL,
   PRIMARY KEY(IdE),
   FOREIGN KEY(IdEns) REFERENCES Enseignant(IdEns)
);

CREATE TABLE Réalisation(
   IdP INT,
   IdReal INT,
   Logo VARCHAR(50),
   NoteFinale DECIMAL(15,2),
   Commentaire VARCHAR(50),
   Nom VARCHAR(50),
   IdE INT NOT NULL,
   PRIMARY KEY(IdP, IdReal),
   FOREIGN KEY(IdP) REFERENCES Projet(IdP),
   FOREIGN KEY(IdE) REFERENCES Equipe(IdE)
);

CREATE TABLE Rendu(
   IdR INT AUTO_INCREMENT,
   DateRendu DATE,
   Version VARCHAR(50),
   Etat VARCHAR(50),
   Noté BOOLEAN,
   Note DECIMAL(15,2),
   IdP INT NOT NULL,
   IdJ INT NOT NULL,
   PRIMARY KEY(IdR),
   FOREIGN KEY(IdP, IdJ) REFERENCES Jalon(IdP, IdJ)
);

CREATE TABLE Concours(
   IdC INT AUTO_INCREMENT,
   Libellé VARCHAR(50),
   Description VARCHAR(50),
   Prix VARCHAR(50),
   PRIMARY KEY(IdC)
);

CREATE TABLE Avancement(
   IdP INT,
   IdJ INT,
   DateInitiale DATE,
   DateRendu DATE,
   Noté BOOLEAN,
   Type VARCHAR(50),
   NoteFinale FLOAT,
   PRIMARY KEY(IdP, IdJ),
   FOREIGN KEY(IdP) REFERENCES Projet(IdP)
);

CREATE TABLE Questionnaire(
   IdP INT,
   IdJ INT,
   DateInitiale DATE,
   DateRendu DATE,
   Noté BOOLEAN,
   Type VARCHAR(50),
   NoteFinale FLOAT,
   thème VARCHAR(50),
   PRIMARY KEY(IdP, IdJ),
   FOREIGN KEY(IdP, IdJ) REFERENCES Jalon(IdP, IdJ)
);

CREATE TABLE Rapport(
   IdP INT,
   IdJ INT,
   DateInitiale DATE,
   DateRendu DATE,
   Noté BOOLEAN,
   Type VARCHAR(50),
   NoteFinale FLOAT,
   titre VARCHAR(50),
   description VARCHAR(50),
   PRIMARY KEY(IdP, IdJ),
   FOREIGN KEY(IdP, IdJ) REFERENCES Jalon(IdP, IdJ)
);

CREATE TABLE Code(
   IdP INT,
   IdJ INT,
   DateInitiale DATE,
   DateRendu DATE,
   Noté BOOLEAN,
   Type VARCHAR(50),
   NoteFinale FLOAT,
   PRIMARY KEY(IdP, IdJ),
   FOREIGN KEY(IdP) REFERENCES Projet(IdP)
);

CREATE TABLE Soutenance(
   IdP INT,
   IdJ INT,
   DateInitiale DATE,
   DateRendu DATE,
   Noté BOOLEAN,
   Type VARCHAR(50),
   NoteFinale FLOAT,
   titre VARCHAR(50),
   consigne VARCHAR(50),
   lieu VARCHAR(50),
   datePassage DATE,
   créneau TIME,
   PRIMARY KEY(IdP, IdJ),
   FOREIGN KEY(IdP) REFERENCES Projet(IdP)
);

CREATE TABLE Question(
   IdQ INT AUTO_INCREMENT,
   thème VARCHAR(50),
   libellé VARCHAR(50),
   PRIMARY KEY(IdQ)
);

CREATE TABLE élément(
   IdElt INT AUTO_INCREMENT,
   texte VARCHAR(50),
   _IdJ VARCHAR(50),
   PRIMARY KEY(IdElt)
);

CREATE TABLE rédiger(
   IdEns INT,
   IdP INT,
   PRIMARY KEY(IdEns, IdP),
   FOREIGN KEY(IdEns) REFERENCES Enseignant(IdEns),
   FOREIGN KEY(IdP) REFERENCES Projet(IdP)
);

CREATE TABLE créer(
   IdP INT,
   IdP_1 INT,
   PRIMARY KEY(IdP, IdP_1),
   FOREIGN KEY(IdP) REFERENCES Projet(IdP),
   FOREIGN KEY(IdP_1) REFERENCES Projet(IdP)
);

CREATE TABLE inscrit(
   NumEtu INT,
   CodeAPOGE VARCHAR(50),
   GroupeTD VARCHAR(50),
   GroupeTP VARCHAR(50),
   PRIMARY KEY(NumEtu, CodeAPOGE),
   FOREIGN KEY(NumEtu) REFERENCES Etudiant(NumEtu),
   FOREIGN KEY(CodeAPOGE) REFERENCES UE(CodeAPOGE)
);

CREATE TABLE appartenir_compose(
   NumEtu INT,
   IdE INT,
   PRIMARY KEY(NumEtu, IdE),
   FOREIGN KEY(NumEtu) REFERENCES Etudiant(NumEtu),
   FOREIGN KEY(IdE) REFERENCES Equipe(IdE)
);

CREATE TABLE composer(
   IdP INT,
   IdReal INT,
   IdR INT,
   PRIMARY KEY(IdP, IdReal, IdR),
   FOREIGN KEY(IdP, IdReal) REFERENCES Réalisation(IdP, IdReal),
   FOREIGN KEY(IdR) REFERENCES Rendu(IdR)
);

CREATE TABLE voter(
   IdEns INT,
   IdP INT,
   IdReal INT,
   IdC INT,
   PRIMARY KEY(IdEns, IdP, IdReal, IdC),
   FOREIGN KEY(IdEns) REFERENCES Enseignant(IdEns),
   FOREIGN KEY(IdP, IdReal) REFERENCES Réalisation(IdP, IdReal),
   FOREIGN KEY(IdC) REFERENCES Concours(IdC)
);

CREATE TABLE dépose(
   NumEtu INT,
   IdR INT,
   PRIMARY KEY(NumEtu, IdR),
   FOREIGN KEY(NumEtu) REFERENCES Etudiant(NumEtu),
   FOREIGN KEY(IdR) REFERENCES Rendu(IdR)
);

CREATE TABLE evalue(
   IdEns INT,
   IdR INT,
   PRIMARY KEY(IdEns, IdR),
   FOREIGN KEY(IdEns) REFERENCES Enseignant(IdEns),
   FOREIGN KEY(IdR) REFERENCES Rendu(IdR)
);

CREATE TABLE pré_requérir(
   IdP INT,
   IdJ INT,
   IdP_1 INT,
   IdJ_1 INT,
   PRIMARY KEY(IdP, IdJ, IdP_1, IdJ_1),
   FOREIGN KEY(IdP, IdJ) REFERENCES Jalon(IdP, IdJ),
   FOREIGN KEY(IdP_1, IdJ_1) REFERENCES Jalon(IdP, IdJ)
);

CREATE TABLE réaliser(
   IdP INT,
   IdE INT,
   PRIMARY KEY(IdP, IdE),
   FOREIGN KEY(IdP) REFERENCES Projet(IdP),
   FOREIGN KEY(IdE) REFERENCES Equipe(IdE)
);

CREATE TABLE contient(
   IdP INT,
   IdJ INT,
   IdQ INT,
   PRIMARY KEY(IdP, IdJ, IdQ),
   FOREIGN KEY(IdP, IdJ) REFERENCES Questionnaire(IdP, IdJ),
   FOREIGN KEY(IdQ) REFERENCES Question(IdQ)
);

CREATE TABLE compose(
   IdP INT,
   IdJ INT,
   IdElt INT,
   PRIMARY KEY(IdP, IdJ, IdElt),
   FOREIGN KEY(IdP, IdJ) REFERENCES Avancement(IdP, IdJ),
   FOREIGN KEY(IdElt) REFERENCES élément(IdElt)
);

CREATE TABLE est_responsable(
   IdEns INT,
   CodeAPOGE VARCHAR(50),
   PRIMARY KEY(IdEns, CodeAPOGE),
   FOREIGN KEY(IdEns) REFERENCES Enseignant(IdEns),
   FOREIGN KEY(CodeAPOGE) REFERENCES UE(CodeAPOGE)
);

CREATE TABLE constituer(
   IdEns INT,
   IdEP INT,
   PRIMARY KEY(IdEns, IdEP),
   FOREIGN KEY(IdEns) REFERENCES Enseignant(IdEns)
);

CREATE TABLE convoque(
   IdE INT,
   IdP INT,
   IdJ INT,
   datePassage VARCHAR(50),
   créneau VARCHAR(50),
   PRIMARY KEY(IdE, IdP, IdJ),
   FOREIGN KEY(IdE) REFERENCES Equipe(IdE),
   FOREIGN KEY(IdP, IdJ) REFERENCES Soutenance(IdP, IdJ)
);

Create View EnsEncadrePlusProjet as 
(select count(eq.IdEns) AS NbDeProjetEncadré,eq.IdEns AS 
IdEns from p1919796.Equipe eq where eq.IdEns in 
(select ens.IdEns from p1919796.Enseignant ens 
where exists(select er.IdEns from p1919796.est_responsable er limit 1)) 
group by eq.IdEns order by count(eq.IdEns) desc);

Create View nbpjt as 
(select u.CodeAPOGE AS CodeAPOGE,u.Libellé AS Libellé,u.Sigle AS Sigle,count(p.IdP) AS Nb 
from p1919796.UE u join p1919796.Projet p join p1919796.est_responsable er join p1919796.Enseignant ens join p1919796.Equipe eq 
join p1919796.réaliser re where u.CodeAPOGE = er.CodeAPOGE and er.IdEns = ens.IdEns 
and eq.IdEns = ens.IdEns and eq.IdE = re.IdE and re.IdP = p.IdP 
group by u.Libellé);