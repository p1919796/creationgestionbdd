#BENZIANE Abdeldjallil p1919796

INSERT INTO Etudiant(NumEtu,Nom,Prénom)
SELECT DISTINCT etudiant1_numetu, SUBSTRING_INDEX(etudiant1_nomprenom,';',-1), SUBSTRING_INDEX(etudiant1_nomprenom,';',1) FROM donnees_fournies.instances WHERE etudiant1_numetu IS NOT NULL AND etudiant1_nomprenom IS NOT NULL
UNION
SELECT DISTINCT etudiant2_numetu, SUBSTRING_INDEX(etudiant2_nomprenom,';',-1), SUBSTRING_INDEX(etudiant2_nomprenom,';',1) FROM donnees_fournies.instances WHERE etudiant2_numetu IS NOT NULL AND etudiant2_nomprenom IS NOT NULL
UNION
SELECT DISTINCT etudiant3_numetu, SUBSTRING_INDEX(etudiant3_nomprenom,';',-1), SUBSTRING_INDEX(etudiant3_nomprenom,';',1) FROM donnees_fournies.instances WHERE etudiant3_numetu IS NOT NULL AND etudiant3_nomprenom IS NOT NULL
UNION
SELECT DISTINCT etudiant4_numetu, SUBSTRING_INDEX(etudiant4_nomprenom,';',-1), SUBSTRING_INDEX(etudiant4_nomprenom,';',1) FROM donnees_fournies.instances WHERE etudiant4_numetu IS NOT NULL AND etudiant4_nomprenom IS NOT NULL
UNION
SELECT DISTINCT etudiant5_numetu, SUBSTRING_INDEX(etudiant5_nomprenom,';',-1), SUBSTRING_INDEX(etudiant5_nomprenom,';',1) FROM donnees_fournies.instances WHERE etudiant5_numetu IS NOT NULL AND etudiant5_nomprenom IS NOT NULL
UNION
SELECT DISTINCT etudiant6_numetu, SUBSTRING_INDEX(etudiant6_nomprenom,';',-1), SUBSTRING_INDEX(etudiant6_nomprenom,';',1) FROM donnees_fournies.instances WHERE etudiant6_numetu IS NOT NULL AND etudiant6_nomprenom IS NOT NULL
UNION
SELECT DISTINCT etudiant7_numetu, SUBSTRING_INDEX(etudiant7_nomprenom,';',-1), SUBSTRING_INDEX(etudiant7_nomprenom,';',1) FROM donnees_fournies.instances WHERE etudiant7_numetu IS NOT NULL AND etudiant7_nomprenom IS NOT NULL
UNION
SELECT DISTINCT etudiant8_numetu, SUBSTRING_INDEX(etudiant8_nomprenom,';',-1), SUBSTRING_INDEX(etudiant8_nomprenom,';',1) FROM donnees_fournies.instances WHERE etudiant8_numetu IS NOT NULL AND etudiant8_nomprenom IS NOT NULL;
#insertion de tous les étudiants (numetu, nom, prenom)


INSERT INTO UE(CodeAPOGE, Libellé, Semestre,NbDeProjet) SELECT DISTINCT code_apoge, ue_libelle, semestre, COUNT(projet_titre) FROM donnees_fournies.instances group by code_apoge;
#pour insérer les données des ue selon leurs code apoge (31)


INSERT INTO inscrit(NumEtu, CodeApoge)
SELECT i.etudiant1_numetu, code_apoge FROM donnees_fournies.instances i WHERE i.etudiant1_numetu IS NOT NULL
UNION
SELECT i.etudiant2_numetu, code_apoge FROM donnees_fournies.instances i WHERE i.etudiant2_numetu IS NOT NULL
UNION
SELECT i.etudiant3_numetu, code_apoge FROM donnees_fournies.instances i WHERE i.etudiant3_numetu IS NOT NULL
UNION
SELECT i.etudiant4_numetu, code_apoge FROM donnees_fournies.instances i WHERE i.etudiant4_numetu IS NOT NULL
UNION
SELECT i.etudiant5_numetu, code_apoge FROM donnees_fournies.instances i WHERE i.etudiant5_numetu IS NOT NULL
UNION
SELECT i.etudiant6_numetu, code_apoge FROM donnees_fournies.instances i WHERE i.etudiant6_numetu IS NOT NULL
UNION
SELECT i.etudiant7_numetu, code_apoge FROM donnees_fournies.instances i WHERE i.etudiant7_numetu IS NOT NULL
UNION
SELECT i.etudiant8_numetu, code_apoge FROM donnees_fournies.instances i WHERE i.etudiant8_numetu IS NOT NULL;
#Insertion des numetu pour codeApoge (2852)







ALTER TABLE Projet AUTO_INCREMENT=1;

INSERT INTO Projet(IdP, Libellé, Semestre, Année) SELECT DISTINCT idp, projet_titre, semestre, annee FROM donnees_fournies.instances;
#insertion dans la table projet des infos idp, libelle(titre), sem et année(155)

UPDATE Projet 
SET Etat = "Archivé"
WHERE Libellé LIKE "%2021%" OR Libellé LIKE "%2020%";

UPDATE Projet 
SET Etat = "Actif"
WHERE Libellé LIKE "%2022%";







INSERT INTO Jalon(IdJ, IdP, DateInitiale, DateRendu, Noté, Type, NoteFinale)
SELECT DISTINCT jalon1_num, idp, jalon1_datelimite, jalon1_datelimite, NULL, jalon1_libelle, jalon1_note FROM donnees_fournies.instances WHERE jalon1_num IS NOT NULL and idp IS NOT NULL
UNION
SELECT DISTINCT jalon2_num, idp, jalon2_datelimite, jalon2_datelimite, NULL, jalon2_libelle, jalon2_note FROM donnees_fournies.instances WHERE jalon2_num IS NOT NULL and idp IS NOT NULL
UNION 
SELECT DISTINCT jalon3_num, idp, jalon3_datelimite, jalon3_datelimite, NULL, jalon3_libelle, jalon3_note FROM donnees_fournies.instances WHERE jalon3_num IS NOT NULL and idp IS NOT NULL
UNION 
SELECT DISTINCT jalon4_num, idp, jalon4_datelimite, jalon4_datelimite, NULL, jalon4_libelle, jalon4_note FROM donnees_fournies.instances WHERE jalon4_num IS NOT NULL and idp IS NOT NULL;
#insertion dans jalon (510)





INSERT INTO Avancement(IdJ, IdP, DateInitiale, DateRendu, Noté, Type, NoteFinale)
SELECT DISTINCT jalon1_num, idp, jalon1_datelimite, jalon1_datelimite, NULL, jalon1_libelle, jalon1_note FROM donnees_fournies.instances WHERE jalon1_num IS NOT NULL and idp IS NOT NULL and jalon1_libelle LIKE "%Avancement%"
UNION
SELECT DISTINCT jalon2_num, idp, jalon2_datelimite, jalon2_datelimite, NULL, jalon2_libelle, jalon2_note FROM donnees_fournies.instances WHERE jalon2_num IS NOT NULL and idp IS NOT NULL and jalon2_libelle LIKE "%Avancement%"
UNION 
SELECT DISTINCT jalon3_num, idp, jalon3_datelimite, jalon3_datelimite, NULL, jalon3_libelle, jalon3_note FROM donnees_fournies.instances WHERE jalon3_num IS NOT NULL and idp IS NOT NULL and jalon3_libelle LIKE "%Avancement%"
UNION 
SELECT DISTINCT jalon4_num, idp, jalon4_datelimite, jalon4_datelimite, NULL, jalon4_libelle, jalon4_note FROM donnees_fournies.instances WHERE jalon4_num IS NOT NULL and idp IS NOT NULL and jalon4_libelle LIKE "%Avancement%";
#Insertion des jalons de type avancement (155)





INSERT INTO Rapport(IdJ, IdP, DateInitiale, DateRendu, Noté, Type, NoteFinale, titre, description)
SELECT DISTINCT jalon1_num, idp, jalon1_datelimite, jalon1_datelimite, NULL, jalon1_libelle, jalon1_note, jalon1_libelle, NULL FROM donnees_fournies.instances WHERE jalon1_num IS NOT NULL and idp IS NOT NULL and jalon1_libelle LIKE "%Rapport%"
UNION
SELECT DISTINCT jalon2_num, idp, jalon2_datelimite, jalon2_datelimite, NULL, jalon2_libelle, jalon2_note, jalon2_libelle, NULL FROM donnees_fournies.instances WHERE jalon2_num IS NOT NULL and idp IS NOT NULL and jalon2_libelle LIKE "%Rapport%"
UNION 
SELECT DISTINCT jalon3_num, idp, jalon3_datelimite, jalon3_datelimite, NULL, jalon3_libelle, jalon3_note, jalon3_libelle, NULL FROM donnees_fournies.instances WHERE jalon3_num IS NOT NULL and idp IS NOT NULL and jalon3_libelle LIKE "%Rapport%"
UNION 
SELECT DISTINCT jalon4_num, idp, jalon4_datelimite, jalon4_datelimite, NULL, jalon4_libelle, jalon4_note, jalon4_libelle, NULL FROM donnees_fournies.instances WHERE jalon4_num IS NOT NULL and idp IS NOT NULL and jalon4_libelle LIKE "%Rapport%";
#Insertion des jalons de type rapport final (155)





INSERT INTO Code(IdJ, IdP, DateInitiale, DateRendu, Noté, Type, NoteFinale)
SELECT DISTINCT jalon1_num, idp, jalon1_datelimite, jalon1_datelimite, NULL, jalon1_libelle, jalon1_note FROM donnees_fournies.instances WHERE jalon1_num IS NOT NULL and idp IS NOT NULL and jalon1_libelle LIKE "%Code%"
UNION
SELECT DISTINCT jalon2_num, idp, jalon2_datelimite, jalon2_datelimite, NULL, jalon2_libelle, jalon2_note FROM donnees_fournies.instances WHERE jalon2_num IS NOT NULL and idp IS NOT NULL and jalon2_libelle LIKE "%Code%"
UNION 
SELECT DISTINCT jalon3_num, idp, jalon3_datelimite, jalon3_datelimite, NULL, jalon3_libelle, jalon3_note FROM donnees_fournies.instances WHERE jalon3_num IS NOT NULL and idp IS NOT NULL and jalon3_libelle LIKE "%Code%"
UNION 
SELECT DISTINCT jalon4_num, idp, jalon4_datelimite, jalon4_datelimite, NULL, jalon4_libelle, jalon4_note FROM donnees_fournies.instances WHERE jalon4_num IS NOT NULL and idp IS NOT NULL and jalon4_libelle LIKE "%Code%";
#Insertion des jalons de type revue de code (45)






INSERT INTO Soutenance(IdJ, IdP, DateInitiale, DateRendu, Noté, Type, NoteFinale, titre, consigne, datePassage, créneau)
SELECT DISTINCT jalon1_num, idp, jalon1_datelimite, jalon1_datelimite, NULL, jalon1_libelle, jalon1_note, jalon1_libelle, NULL, NULL, NULL FROM donnees_fournies.instances WHERE jalon1_num IS NOT NULL and idp IS NOT NULL and jalon1_libelle LIKE "%Soutenance%"
UNION
SELECT DISTINCT jalon2_num, idp, jalon2_datelimite, jalon2_datelimite, NULL, jalon2_libelle, jalon1_note, jalon2_libelle, NULL, NULL, NULL FROM donnees_fournies.instances WHERE jalon2_num IS NOT NULL and idp IS NOT NULL and jalon2_libelle LIKE "%Soutenance%"
UNION 
SELECT DISTINCT jalon3_num, idp, jalon3_datelimite, jalon3_datelimite, NULL, jalon3_libelle, jalon3_note, jalon3_libelle, NULL, NULL, NULL FROM donnees_fournies.instances WHERE jalon3_num IS NOT NULL and idp IS NOT NULL and jalon3_libelle LIKE "%Soutenance%"
UNION 
SELECT DISTINCT jalon4_num, idp, jalon4_datelimite, jalon4_datelimite, NULL, jalon4_libelle, jalon4_note, jalon4_libelle, NULL, NULL, NULL FROM donnees_fournies.instances WHERE jalon4_num IS NOT NULL and idp IS NOT NULL and jalon4_libelle LIKE "%Soutenance%";
#Insertion des jalons de type Soutenance (155)






ALTER TABLE Rendu AUTO_INCREMENT=1;

INSERT INTO Rendu(DateRendu, Etat, Note, Noté, IdP, IdJ)
SELECT DISTINCT rendu1_date, "Rendu", rendu1_note, 1, idp, jalon1_num FROM donnees_fournies.instances WHERE jalon1_num IS NOT NULL AND rendu1_note IS NOT NULL
UNION
SELECT DISTINCT rendu2_date, "Rendu", rendu2_note, 1, idp, jalon2_num FROM donnees_fournies.instances WHERE jalon2_num IS NOT NULL AND rendu2_note IS NOT NULL
UNION
SELECT DISTINCT rendu3_date, "Rendu", rendu3_note, 1, idp, jalon3_num FROM donnees_fournies.instances WHERE jalon3_num IS NOT NULL AND rendu3_note IS NOT NULL
UNION
SELECT DISTINCT rendu4_date, "Rendu", rendu4_note, 1, idp, jalon4_num FROM donnees_fournies.instances WHERE jalon4_num IS NOT NULL AND rendu4_note IS NOT NULL ;
#Insertion des rendus notés



INSERT INTO Rendu(DateRendu, Etat, Note, Noté, IdP, IdJ)
SELECT DISTINCT rendu1_date, "En attente", rendu1_note, 0, idp, jalon1_num FROM donnees_fournies.instances WHERE jalon1_num IS NOT NULL AND rendu1_note IS NULL AND datediff(rendu1_date, date(now())) > 0
UNION
SELECT DISTINCT rendu2_date, "En attente", rendu2_note, 0,  idp, jalon2_num FROM donnees_fournies.instances WHERE jalon2_num IS NOT NULL AND rendu2_note IS NULL AND datediff(rendu2_date, date(now())) > 0
UNION
SELECT DISTINCT rendu3_date, "En attente", rendu3_note, 0,  idp, jalon3_num FROM donnees_fournies.instances WHERE jalon3_num IS NOT NULL AND rendu3_note IS NULL AND datediff(rendu3_date, date(now())) > 0
UNION
SELECT DISTINCT rendu4_date, "En attente", rendu4_note, 0,  idp, jalon4_num FROM donnees_fournies.instances WHERE jalon4_num IS NOT NULL AND rendu4_note IS NULL AND datediff(rendu4_date, date(now())) > 0;


ALTER TABLE Enseignant AUTO_INCREMENT=1;

INSERT INTO Enseignant(Nom, Prénom)
SELECT DISTINCT ue_responsable_nom, ue_responsable_prenom FROM donnees_fournies.instances
UNION
SELECT DISTINCT encadrant_nom, encadrant_prenom FROM donnees_fournies.instances;
#insertion des noms et prenoms des enseignants

ALTER TABLE Enseignant AUTO_INCREMENT=280;





INSERT INTO est_responsable(CodeAPOGE, IdEns)
SELECT DISTINCT code_apoge, e.IdEns FROM donnees_fournies.instances, Enseignant e WHERE ue_responsable_nom = e.Nom and ue_responsable_prenom = e.Prénom;
#ajout des responsables dans la table est_responsables





ALTER TABLE Equipe AUTO_INCREMENT=1;

INSERT INTO Equipe(Nom, IdEns)
SELECT nom_equipe, ens.IdEns FROM donnees_fournies.instances, Enseignant ens WHERE ens.Nom = encadrant_nom and ens.Prénom = encadrant_prenom;
#insertion des tuples dans equipe avec IdEns et nom des equipes





ALTER TABLE Réalisation AUTO_INCREMENT=1;

insert into Réalisation(IdE, IdP, Nom, NoteFinale) 
SELECT eq.IdE, idp, titre_realisation, note_finale FROM donnees_fournies.instances, Equipe eq WHERE nom_equipe = eq.Nom;
#insertion des réalisation et leurs noms (1126)


ALTER TABLE constituer auto_increment=2;

INSERT INTO constituer(IdEns, IdEP)
SELECT DISTINCT ens.IdEns, 1 FROM donnees_fournies.instances, Enseignant ens WHERE ens.Nom = ue_responsable_nom AND ens.Prénom = ue_responsable_prenom;
#insertion de l'équipe pédagogique pour l'équipe 1






INSERT INTO appartenir_compose(NumEtu, IdE)
SELECT i.etudiant1_numetu, e.IdE FROM donnees_fournies.instances i, Equipe e WHERE e.Nom = i.nom_equipe and i.etudiant1_numetu IS NOT NULL 
UNION 
SELECT i.etudiant2_numetu, e.IdE FROM donnees_fournies.instances i, Equipe e WHERE e.Nom = i.nom_equipe and i.etudiant2_numetu IS NOT NULL
UNION
SELECT i.etudiant3_numetu, e.IdE FROM donnees_fournies.instances i, Equipe e WHERE e.Nom = i.nom_equipe and i.etudiant3_numetu IS NOT NULL
UNION
SELECT i.etudiant4_numetu, e.IdE FROM donnees_fournies.instances i, Equipe e WHERE e.Nom = i.nom_equipe and i.etudiant4_numetu IS NOT NULL
UNION
SELECT i.etudiant5_numetu, e.IdE FROM donnees_fournies.instances i, Equipe e WHERE e.Nom = i.nom_equipe and i.etudiant5_numetu IS NOT NULL
UNION
SELECT i.etudiant6_numetu, e.IdE FROM donnees_fournies.instances i, Equipe e WHERE e.Nom = i.nom_equipe and i.etudiant6_numetu IS NOT NULL
UNION
SELECT i.etudiant7_numetu, e.IdE FROM donnees_fournies.instances i, Equipe e WHERE e.Nom = i.nom_equipe and i.etudiant7_numetu IS NOT NULL
UNION
SELECT i.etudiant8_numetu, e.IdE FROM donnees_fournies.instances i, Equipe e WHERE e.Nom = i.nom_equipe and i.etudiant8_numetu IS NOT NULL;
#Insertion des numeros etu en fonction de leur equipes (2638)




INSERT INTO réaliser(IdE, IdP)
SELECT eq.IdE, idp FROM donnees_fournies.instances, Equipe eq WHERE eq.Nom = nom_equipe;
#insertion des equipe et leurs projets réalisés (1126)

ALTER TABLE élément auto_increment=1;